export const ADD_MESSAGE = 'ADD_MESSAGE'
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE'
export const DELETE_MESSAGE = 'DELETE_MESSAGE'
export const LIKED = 'LIKED'
export const SHOW_EDIT = 'SHOW_EDIT'
export const HIDE_EDIT = 'HIDE_EDIT'

export interface AddMessage {
    type: typeof ADD_MESSAGE;
    payload: { text: string };
}
export interface EditMessage {
    type: typeof UPDATE_MESSAGE;
    payload: { text: string, id: string }
}
export interface DeleteMessage {
    type: typeof DELETE_MESSAGE;
    payload: { id: string };
}

export interface Liked {
    type: typeof LIKED;
    payload: { userId: string, id: string, isLike: Boolean };
}
export interface ShowEdit {
    type: typeof SHOW_EDIT;
    payload: { text: string, id: string }
}
export interface HideEdit {
    type: typeof HIDE_EDIT;
    payload: null;
}

export type ChatActionTypes = AddMessage | EditMessage | DeleteMessage | Liked | ShowEdit | HideEdit