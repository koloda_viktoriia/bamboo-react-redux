import { createStore, combineReducers } from 'redux';
import data from '../data.json';
import userProfile from '../user.json';
import { v4 as uuidv4 } from 'uuid';

import { ChatState, Message, Like, editMSG } from '../types'
import { ChatActionTypes } from './actionTypes'
import { liked } from './actions';

const INITIAL_STATE: ChatState = {
    messages: data,
    likes: [] as Like[],
    userProfile,
    isEdit: false,
    editMessage: {} as editMSG
};

const chatStore = (state = INITIAL_STATE, action: ChatActionTypes): ChatState => {
    switch (action.type) {
        case 'ADD_MESSAGE':
            const { text } = action.payload;
            const message = {
                id: uuidv4(),
                userId: userProfile.userId,
                avatar: userProfile.avatar,
                user: userProfile.user,
                text: text,
                createdAt: new Date().getTime(),
                editedAt: ""
            }
            return { ...state, messages: [...state.messages, message] };
        case 'UPDATE_MESSAGE':
            {
                const { text, id } = action.payload;
                const editedMessage = (message: Message) => (
                    {
                        ...message,
                        text,
                        editedAt: new Date().getTime()
                    });
                const updatedMessages = state.messages.map(message =>
                    message.id === id ? editedMessage(message) : message);
                return { ...state, messages: updatedMessages };
            }
        case 'DELETE_MESSAGE':
            {
                const { id } = action.payload;
                const newMessages = state.messages.filter(message => message.id !== id);
                return { ...state, messages: newMessages };
            }
        case 'LIKED':
            {
                const { userId, id, isLike } = action.payload;
                if (isLike) {
                    const newLikes: Like[] = state.likes
                        .filter((like) =>
                            (like.id !== id) && (like.userId !== userId));
                    return {
                        ...state,
                        likes: newLikes
                    };
                } else {
                    const like =
                    {
                        userId: userId,
                        id: id
                    };
                    return { ...state, likes: [...state.likes, like] };
                }
            }
        case 'SHOW_EDIT': {
            const editMessage = action.payload;
            return { ...state, isEdit: true, editMessage: editMessage };
        }
        case 'HIDE_EDIT':
            return { ...state, isEdit: false };
        default:
            return state;
    }
}

const store = createStore(chatStore);

export default store;