
export const addMessage = (text: string) => {
    return { type: 'ADD_MESSAGE', payload: { text } };
}
export const editMessage = (text: string, id: string) => {
    return { type: 'UPDATE_MESSAGE', payload: { text, id } };

}
export const deleteMessage = (id: string) => {
    return { type: 'DELETE_MESSAGE', payload: { id } };

}
export const liked = (userId: string, id: string, isLike: Boolean) => {
    return { type: 'LIKED', payload: { userId, id, isLike } };
}
export const showEdit = (text: string, id: string) => {
    return { type: 'SHOW_EDIT', payload: { text, id } };

}
export const hideEdit = () => {
    return { type: 'HIDE_EDIT', payload: {} }
}