import React from 'react';
import './Message.css';
import * as types from '../../types';

const editTool = require('./editTool.svg') as string;
const trashTool = require('./trashTool.svg') as string;
const heart = require('./heart.svg') as string;
const emptyHeart = require('./emptyHeart.svg') as string;

interface MessageProps {
    isLike: Boolean;
    msg: types.Message;
    deleteMessage: (id: string) => void;
    showEdit: (text?: string, id?: string) => void;
    liked: (userId: string, id: string, isLike: Boolean) => void;
    userId: string;
}

const Message = (props: MessageProps) => {

    const handleDelete = () => {
        props.deleteMessage(props.msg.id);
    }


    const handleEdit = () => {
        props.showEdit(props.msg.text, props.msg.id);
    }

    const handleIsLike = () => {
        props.liked(props.userId, props.msg.id, props.isLike);
    }

    return (
        <div className={props.msg.userId === props.userId ? "Message Message-user" : "Message"}>
            <span>
                {new Date(props.msg.createdAt).toLocaleString().split(",")[1]}
            </span>
            <div className="Message-box">
                {props.msg.userId !== props.userId &&
                    <img src={props.msg.avatar} className="User-avatar" />}
                <p>
                    {props.msg.text}

                    {props.msg.userId === props.userId &&
                        <div className="Message-tools">
                            <label onClick={handleEdit} >
                                <img src={editTool} className="Icon" />
                            </label>
                            <label onClick={handleDelete}>
                                <img src={trashTool} className="Icon" />
                            </label>
                        </div>
                    }
                </p>
                {props.msg.userId !== props.userId &&
                    <label onClick={handleIsLike} className="Like-icon">
                        {props.isLike ?
                            <img src={heart} className="Like-icon" /> :
                            <img src={emptyHeart} className="Like-icon" />}
                    </label>
                }
            </div>
        </div >
    );
}

export default Message;
