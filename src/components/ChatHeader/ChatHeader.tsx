import React from 'react';
import './ChatHeader.css';
import * as types from '../../types';

const origami = require('./origami.svg') as string;
const kasa = require('./kasa.svg') as string;

interface ChatHeaderProps {
  data: types.Message[]
}

const ChatHeader = (props: ChatHeaderProps) => {
  const messagesCount = props.data?.length;

  const usersCounter = () => {
    const usersId: Array<string> = [];
    props.data?.map(message => {
      if (usersId.indexOf(message.userId) === -1) {
        usersId.push(message.userId)
      }
    });
    return (usersId.length);
  };

  const convertTime = (lastDate: any) => {
    const now = new Date().getTime();
    console.log(now);
    console.log(new Date(lastDate).getTime());
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (new Date(now).toLocaleDateString() === new Date(lastDate).toLocaleDateString()) {
      const time = new Date(lastDate).toLocaleString().split(",")[1].split(":");
      console.log(time);
      return (time[0] + ":" + time[1]);
    } else if (new Date(now).getFullYear() === new Date(lastDate).getFullYear()) {
      const indexMonth = new Date(lastDate).getMonth();
      return (months[indexMonth] + " " + new Date(lastDate).getDate());
    }
    else {
      const time = new Date(lastDate).toLocaleDateString();
      return (time);
    }
  }

  let lastMessage = props.data? props.data[props.data.length - 1]: undefined;
  

  return (
    <div className="Chat-header">
      <span className="Chat-users Header-item">
        <img src={kasa} className="Chat-icon" />
        {usersCounter()}
      </span>
      <span className="Chat-messages Header-item">
        <img src={origami} className="Chat-icon" />
        {messagesCount}
      </span>
      <span className="Chat-title ">Panda's team chat</span>
      {lastMessage &&
        <span className="Chat-last Header-item"> Last message at {convertTime(lastMessage.createdAt)}</span>
      }
    </div>
  );
}

export default ChatHeader;
