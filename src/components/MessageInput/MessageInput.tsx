import React, { useState } from 'react';
import './MessageInput.css';

interface MessageInputProps {
  addMessage: (text: string) => void;
  showEdit: (text?: string, id?: string) => void
}

const MessageInput = (props: MessageInputProps) => {
  const [text, setText] = useState('');

  const handleAddMessage = (event: any) => {
    event.preventDefault();
    if (text !== '') {
      props.addMessage(text);
      setText('');
    }
  }

  const handleEditMessage = (event: any) => {
    event.preventDefault();
    props.showEdit();
  }

  const handleKeyPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (event.key === 'Enter' && event.shiftKey) {
      handleAddMessage(event);
    }
  }

  const handleKeyDown = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (event.key === 'ArrowUp' && text === '') {
      handleEditMessage(event);
    }
  }

  return (
    <div className="Message-input">
      <textarea
        onChange={ev => setText(ev.target.value)}
        onKeyPress={ev => handleKeyPress(ev)}
        onKeyDown={ev => handleKeyDown(ev)}
        value={text}
        placeholder="Type your message and push button 'Send' or press 'Shift+Enter'" />
      <div className="Input-button">
        <button onClick={ev => handleAddMessage(ev)} >Send</button>
      </div>
    </div>
  );
}


export default MessageInput;
